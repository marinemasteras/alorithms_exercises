package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MarsExploration {

  static int marsExploration(String s) {
    int counter = s.length();
    int i = 0;
    while (i < s.length()) {
      for (int j = 0; j < 3; j++) {
        if (j == 1 && s.charAt(i) == 'O') {
          counter--;
        }
        else if (j != 1 && s.charAt(i) == 'S') {
          counter--;
        }
        i++;
      }
    }
    return counter;
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));

    String s = scanner.nextLine();

    int result = marsExploration(s);

    System.out.println(result);
  }
}
