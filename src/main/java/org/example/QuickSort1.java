package org.example;

import java.util.Arrays;

public class QuickSort1 {


  static int[] quickSort1(int[] arr) {
    //int countSwaps=0;
    int p = arr[0];
    int left = 1;
    int right = arr.length - 1;
    while (left < right) {
      while (left < arr.length && arr[left] < p) {
        left++;
      }
      while (arr[right] > p) {
        right--;
      }
      if (left < right) {
        //countSwaps++;
        swap(arr, left, right);
      }
    }
    if (p > arr[right]) {
      //countSwaps++;
      swap(arr, 0, right);
    }
    return arr;
    //return new int[]{countSwaps};
  }
  public static int[] quickSort (int[]arr){
    int lo = 0;
        int hi = arr.length-1;
        quickSort(arr,lo,hi);
        return arr;
  }


  static void quickSort(int[] arr, int lo, int hi) {
    if (hi<=lo) return;
    int j = partition(arr,lo,hi);
    quickSort(arr, lo, j-1);
    quickSort(arr,j+1,hi);
  }

  private static int partition(int[] a, int lo, int hi){
    int i = lo, j = hi+1;
    while (true){
      while(a[++i]<a[lo]){
        if(i==hi) break;
      }
      while (a[lo]<a[--j]){
        if(j==lo) break;
      }
      if (i >= j) break;
      swap(a,i,j);
    }
    swap(a, lo, j);
    return j;
  }

  static int[] quickSort2(int[] arr) {
    int countSwaps=0;
    int pivot = arr[0];
    int n = arr.length;
    for (int i = 1; i < n; i++) {
      if (pivot > arr[i]) {
        for (int j = i; j > 0; j--) {
          countSwaps++;
          int temp = arr[j];
          arr[j] = arr[j - 1];
          arr[j - 1] = temp;
        }
      }
    }
    return arr;
    //return new int[]{countSwaps};
  }

  private static void swap(int[] arr, int a, int b) {
    int copy = arr[a];
    arr[a] = arr[b];
    arr[b] = copy;
  }

  public static void main(String[] args) {
    System.out.println(Arrays.toString(quickSort(new int[]{6, 9, 8, 4, 5, 3, 7, 2})));
    System.out.println(Arrays.toString(quickSort1(new int[]{6, 9, 8, 4, 5, 3, 7, 2})));
    System.out.println(Arrays.toString(quickSort2(new int[]{6, 9, 8, 4, 5, 3, 7, 2})));
  }
}
