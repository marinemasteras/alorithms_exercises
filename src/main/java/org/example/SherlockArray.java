package org.example;

import java.util.Arrays;
import java.util.List;

public class SherlockArray {
  static String balancedSums(List<Integer> arr) {
    if (arr.size() == 1) {
      return "YES";
    }
    int sumLeft = 0;
    int sumRight = arr.subList(1,arr.size()).stream()
        .reduce(0,Integer::sum);

    for (int i = 0; i<arr.size()-1; i++){
      if (sumLeft==sumRight){
        return "YES";
      }
      sumLeft+=arr.get(i);
      sumRight-=arr.get(i+1);
    }
    return "NO";
  }

  public static void main(String[] args) {
    System.out.println(balancedSums(Arrays.asList(1,11,1000,1,1,1,1,1,1,1,1,1,1,1,1)));
  }
}
