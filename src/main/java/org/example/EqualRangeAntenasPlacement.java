package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class EqualRangeAntenasPlacement {

  public EqualRangeAntenasPlacement() {
  }

  static int hackerlandRadioTransmitters(int[] x, int k) {
    Arrays.sort(x);
    int i = 0;
    int counter = 0;
    int transmitterPosition;

    while (i < x.length) {
      counter++;
      transmitterPosition = x[i] + k;
      while (i < x.length && x[i] <= transmitterPosition) {
        i++;
      }
      transmitterPosition = x[--i] + k;
      while (i < x.length && x[i] <= transmitterPosition) {
        i++;
      }
    }
    System.out.println(counter);
    return counter;
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input04.txt"));
    int n = scanner.nextInt();
    int k = scanner.nextInt();
    int[] arr = new int[n];
    for (int i = 0; i < n; i++) {
      arr[i] = scanner.nextInt();
    }
    hackerlandRadioTransmitters(arr, k);

    Scanner scanner2 = new Scanner(new File("src/input05.txt"));
    int n2 = scanner2.nextInt();
    int k2 = scanner2.nextInt();
    int[] arr2 = new int[n2];
    for (int i = 0; i < n2; i++) {
      arr2[i] = scanner2.nextInt();
    }
    hackerlandRadioTransmitters(arr2, k2);
  }


}
