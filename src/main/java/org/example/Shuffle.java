package org.example;

import java.util.Arrays;
import java.util.Random;

public class Shuffle {

  private static Random random;



  public static int random (int i){
   return random.nextInt(i);
    }

  public static void shuffle (int[] arr){
    for (int i = 1; i<arr.length; i++){
      swap(arr, arr[i], arr[random.nextInt(i)]);
    }
  }

  private static void swap(int[] arr, int a, int b) {
    int copy = arr[a];
    arr[a] = arr[b];
    arr[b] = copy;
  }

  public static void main(String[] args) {
    int[] a = new int[]{1,2,3,4,5,6,7,8,9};
    int[] b = new int[]{1,2,3,4,5,6,7,8,9};
    int[] c = new int[]{1,2,3,4,5,6,7,8,9};
    int[] d = new int[]{1,2,3,4,5,6,7,8,9};
    shuffle(a);
    shuffle(b);
    System.out.println(Arrays.toString(a));
    System.out.println(Arrays.toString(b));
    for (int i=0; i< c.length;i++){
      c[i] = random(c[i]);
    }
    System.out.println(Arrays.toString(c));

    for (int i=0; i< d.length;i++){
      d[i] = random(d[i]);
    }
    System.out.println(Arrays.toString(d));
  }
}
