package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class ConnectedCell {

  static int connectedCell(int[][] matrix) {
    int n = matrix.length;
    int m = matrix[0].length;
    int max = 1;
    int[] parent = new int[m * n];
    int[] size = new int[m * n];
    for (int i = 0; i < m * n; i++) {
      parent[i] = i;
      size[i] = 1;
    }
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < m; j++) {
        if (matrix[i][j] == 1) {
          if (i > 0) {
            if (matrix[i - 1][j] == 1) {
              max = union(i * m + j, (i - 1) * m + j, parent, size, max);
            }
            if (j > 0 && matrix[i - 1][j - 1] == 1) {
              max = union(i * m + j, (i - 1) * m + j - 1, parent, size, max);
            }
            if (j < m - 1 && matrix[i - 1][j + 1] == 1) {
              max = union(i * m + j, (i - 1) * m + j + 1, parent, size, max);
            }
          }
          if (i < n - 1) {
            if (matrix[i + 1][j] == 1) {
              max = union(i * m + j, (i + 1) * m + j, parent, size, max);
            }
            if (j > 0 && matrix[i + 1][j - 1] == 1) {
              max = union(i * m + j, (i + 1) * m + j - 1, parent, size, max);
            }
            if (j < m - 1 && matrix[i + 1][j + 1] == 1) {
              max = union(i * m + j, (i + 1) * m + j + 1, parent, size, max);
            }
          }
          if (j > 0 && matrix[i][j - 1] == 1) {
            max = union(i * m + j, i * m + j - 1, parent, size, max);
          }
          if (j < m - 1 && matrix[i][j + 1] == 1) {
            max = union(i * m + j, i * m + j + 1, parent, size, max);
          }
        }
      }
    }
    return max;
  }

  private static int find(int p, int[] parent) {
    while (p != parent[p]) {
      p = parent[p];
    }
    return p;
  }

  private static int union(int p, int q, int[] parent, int[] size, int max) {
    int rootP = find(p, parent);
    int rootQ = find(q, parent);
    if (rootP == rootQ) {
      return max;
    }

    if (size[rootP] < size[rootQ]) {
      parent[rootP] = rootQ;
      size[rootQ] += size[rootP];
      if (size[rootQ] >= max) {
        max = size[rootQ];
      }
    } else {
      parent[rootQ] = rootP;
      size[rootP] += size[rootQ];
      if (size[rootP] >= max) {
        max = size[rootP];
      }
    }
    return max;
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input04.txt"));
    int n = scanner.nextInt();
    int x = scanner.nextInt();
    int[][] arr = new int[n][x];
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < x; j++) {
        arr[i][j] = scanner.nextInt();
      }
    }
    connectedCell(arr);
  }
}
