package org.example;

public class InsertionSort2 {


  static void insertionSort2(int n, int[] arr) {
    for (int i = 1; i < n ; i++) {
      int valueI = arr[i];
      int j = i - 1;
      while (j > -1 && arr[j] > valueI) {
        int valueY = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = valueY;
        j--;
      }
      printOutput(arr);
    }
  }

  static void insertionSort3(int n, int[] arr) {
    for (int i = 1; i < n ; i++) {
      int valueI = arr[i];
      int j = i - 1;
      while (j > -1 && arr[j] > valueI) {
        arr[j+1] = arr[j];
        j--;
        arr[j+1] = valueI;
      }

    }
    printOutput(arr);
  }

  static int runningTime(int[] arr) {
    int count=0;
    for(int i = 1; i< arr.length; i++){
      int value = arr[i];
      int j = i-1;
      while(j> -1 && arr[j]>value){
        count++;
        arr[j+1]=arr[j];
        j--;
        arr[j+1]=value;
      }
    }
    return count;
  }

  private static void printOutput(int[] arr) {
    StringBuilder s = new StringBuilder();
    for (int i : arr) {
      s.append(i).append(" ");
    }
    System.out.println(s);
  }

  public static void main(String[] args) {
    insertionSort2(6, new int[]{1, 4, 3, 5, 6, 2});
    insertionSort3(6, new int[]{1, 4, 3, 5, 6, 2});
  }
}