package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SeparateNumbers {

  static void separateNumbers(String s) {
    if (s.charAt(0) == '0') {
      System.out.println("NO");
      return;
    }
    int maxN = s.length() / 2;
    for (int i = 1; i < maxN + 1; i++) {
      int index = i;
      String beginnerString = s.substring(0, index);
      int numberLength = beginnerString.length();
      long beginner = Long.parseLong(beginnerString);

      while (index + numberLength <= s.length()) {
        if (s.charAt(index) == '0') {
          break;
        }
        String seconderString =
            (beginner + 1) % 10 == 0 && beginner % 9 == 0 && index + numberLength != s.length() ? s
                .substring(index, ++numberLength + index)
                : s.substring(index, index + numberLength);
        long seconder = Long.parseLong(seconderString);
        if (seconder - beginner != 1) {
          break;
        }
        index = numberLength + index;
        if (index == s.length()) {
          System.out.println("YES " + s.substring(0, i));
          return;
        }
        beginner = seconder;
      }

    }
    System.out.println("NO");
  }

  public static void main(String[] args) throws FileNotFoundException {
    separateNumbers("42949672954294967296429496729");
    Scanner scanner = new Scanner(new File("src/input00.txt"));
    int q = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    for (int qItr = 0; qItr < q; qItr++) {
      String s = scanner.nextLine();
      separateNumbers(s);
    }
  }
}
