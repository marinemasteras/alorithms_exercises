package org.example;

public class HackerrankInString {

  static String hackerrankInString(String s) {
    int i = 0;
    int j = 0;
    String name = "hackerrank";
    while (i < s.length()) {
      if (name.charAt(j) == s.charAt(i)) {
        j++;
        if (j == name.length()) {
          return "YES";
        }
      }
      i++;
    }
    return "NO";
  }
}
