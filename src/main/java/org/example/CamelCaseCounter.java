package org.example;

public class CamelCaseCounter {

  static int camelcase(String s) {
    int counter = 1;
    for (int i = 1; i < s.length(); i++) {
      char ch = s.charAt(i);
      if (ch < 91) {
        counter++;
      }
    }
    return counter;
  }
}
