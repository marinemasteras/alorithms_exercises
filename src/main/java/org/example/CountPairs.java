package org.example;

import java.util.Arrays;

public class CountPairs {

  static int pairs(int k, int[] arr) {
    Arrays.sort(arr);
    int pointerI = 0;
    int pointerY = 1;
    int counter = 0;

    for (int j = pointerI; j < arr.length; j++) {
      for (int i = pointerY; i < arr.length; i++) {
        if (arr[i] - arr[j] == k) {
          counter++;
          pointerY = i + 1;
          break;
        }
      }
    }
    return counter;
  }

  static int pairs2(int k, int[] arr) {
    Arrays.sort(arr);
    int pointerI = 0;
    int pointerY = 1;
    int counter = 0;
    while (pointerY < arr.length) {
      if (arr[pointerY] - arr[pointerI] == k) {
        counter++;
        pointerY++;
      } else if (arr[pointerY] - arr[pointerI] < k) {
        pointerY++;
      } else if (arr[pointerY] - arr[pointerI] > k){
        pointerI++;
      }
    }
    return counter;

  }

  public static void main(String[] args) {
    System.out.println(pairs(2,new int[]{1, 5, 3, 4, 2}));
    System.out.println(pairs2(2,new int[]{1, 5, 3, 4, 2}));
  }

}
