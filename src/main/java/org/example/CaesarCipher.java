package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CaesarCipher {

  static String caesarCipher(String s, int k) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int index = 0; index < s.length(); index++) {
      char ch = s.charAt(index);
      if (isLetter(ch)) {
        ch = cipherCharBy(ch, k);
      }
      stringBuilder.append(ch);
    }
    return stringBuilder.toString();
  }

  static String caesarCipherInPlace(String s, int k) {
    StringBuilder stringBuilder = new StringBuilder(s);
    for (int index = 0; index < s.length(); index++) {
      final char tmp = stringBuilder.charAt(index);
      if (isLetter(tmp)) {
        stringBuilder.setCharAt(index, cipherCharBy(tmp, k));
      }
    }
    return stringBuilder.toString();
  }

  private static boolean isLetter(char ch) {
    return (ch > 64 && ch < 91) || (ch > 96 && ch < 123);
  }

  private static char cipherCharBy(char ch, int k) {
    if (ch > 96) {
      return (char) (((ch - 97 + k) % 26) + 97);
    } else {
      return (char) (((ch - 65 + k) % 26) + 65);
    }
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));

    int n = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    String s = scanner.nextLine();

    int k = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    String result = caesarCipher(s, k);
    System.out.println(result);

    String result2 = caesarCipherInPlace(s, k);
    System.out.println(result2);
  }
}
