package org.example;

import java.util.Arrays;

public class CountingSort2 {

  static int[] countingSort(int[] arr) {
    int[] sortedIndexes = new int[100];
    int max = 0;
    for (int i : arr) {
      if (i > max) {
        max = i;
      }
      sortedIndexes[i]++;
    }
    int k = 0;
    for (int i = 0; i <= max; i++) {
      if (sortedIndexes[i] > 0) {
        while (sortedIndexes[i] > 0) {
          arr[k++] = i;
          sortedIndexes[i]--;
        }
      }
    }
    return arr;
  }

  public static void main(String[] args) {
    System.out.println(
        Arrays.toString(countingSort(new int[]{19, 10, 12, 10, 24, 25, 22})));
  }


}
