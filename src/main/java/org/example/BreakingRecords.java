package org.example;

import java.util.Arrays;

public class BreakingRecords {

  public static void main(String[] args) {
    System.out.println(Arrays.toString(breakingRecords(new int[]{10, 5, 20, 20, 4, 5, 2, 25, 1})));
  }

  static int[] breakingRecords(int[] scores) {

    if (scores.length == 0) {
      return new int[]{0, 0};
    }
    int max = scores[0];
    int min = max;
    int minCount = 0;
    int maxCount = 0;
    int index = 0;

    while (index < scores.length) {
      if (scores[index] > max) {
        max = scores[index];
        maxCount++;
      } else if (scores[index] < min) {
        min = scores[index];
        minCount++;
      }
      index++;
    }
    return new int[]{maxCount, minCount};
  }
}
