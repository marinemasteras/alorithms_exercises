package org.example;

import java.util.Arrays;
import java.util.Random;

public class Median {


  static int findMedianNlogN(int[] arr) {
    Arrays.sort(arr);
    return arr[arr.length / 2];
  }

  static int findMedian(int[] arr) {
    int[] copy = Arrays.copyOf(arr, arr.length);
    return quickSelectRecursive(copy, 0, copy.length - 1);

  }

  static int quickSelectRecursive(int[] arr, int lo, int hi) {
    int j = partition(arr, lo, hi);
    System.out.println("recursive j " + j);
    if (j > arr.length / 2) {
      quickSelectRecursive(arr, lo, j - 1);
    } else if (j < arr.length / 2) {
      quickSelectRecursive(arr, j + 1, hi);
    }
    System.out.println(Arrays.toString(arr));
    return arr[arr.length / 2];
  }

  static int findMedianQuickSelect(int[] arr) {
    int[] copy = Arrays.copyOf(arr, arr.length);
    return quickSelect(copy, copy.length / 2);
  }

  static int quickSelect(int[] a, int k) {
    shuffle(a);
    int lo = 0, hi = a.length - 1;
    while (hi > lo) {
      int j = partition(a, lo, hi);
      System.out.println("looped j " + j);
      if (k > j) {
        lo = j + 1;
      } else if (k < j) {
        hi = j - 1;
      } else {
        System.out.println(Arrays.toString(a));
        return a[k];
      }
    }
    return a[k];
  }

  private static int partition(int[] a, int lo, int hi) {
    int i = lo, j = hi + 1;
    while (true) {
      while (a[++i] < a[lo]) {
        if (i == hi) {
          break;
        }
      }
      while (a[lo] < a[--j]) {
      }
      if (i >= j) {
        break;
      }
      swap(a, i, j);
    }
    swap(a, lo, j);
    return j;
  }

  private static void swap(int[] arr, int a, int b) {
    int copy = arr[a];
    arr[a] = arr[b];
    arr[b] = copy;
  }

  private static void shuffle(int[] arr) {
    Random random = new Random();
    for (int i = 0; i < arr.length; ++i) {
      swap(arr, arr[i], arr[i+ random.nextInt(arr.length - i)]);
    }
  }

  public static void main(String[] args) {
    //System.out.println(findMedian2(new int[]{5, 4, 3, 2, 2, 1, 1, 0}));
    //System.out.println(findMedian(new int[]{5, 4, 3, 2, 2, 1, 1, 0}));
    System.out.println(findMedian(new int[]{5, 4, 3, 2, 4, 1, 1, 2}));
    //System.out.println(findMedian3(new int[]{5, 2, 3, 1, 4, 8, 9, 7}));
    System.out.println(findMedianQuickSelect(new int[]{5, 4, 3, 2, 1, 0, 7, 6}));
  }
}
