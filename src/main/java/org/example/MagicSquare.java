package org.example;

public class MagicSquare {

  static int formingMagicSquare(int[][] s) {
    int magicSquares[][][] = {
        {{8, 1, 6}, {3, 5, 7}, {4, 9, 2}},
        {{6, 1, 8}, {7, 5, 3}, {2, 9, 4}},
        {{4, 9, 2}, {3, 5, 7}, {8, 1, 6}},
        {{2, 9, 4}, {7, 5, 3}, {6, 1, 8}},
        {{8, 3, 4}, {1, 5, 9}, {6, 7, 2}},
        {{4, 3, 8}, {9, 5, 1}, {2, 7, 6}},
        {{6, 7, 2}, {1, 5, 9}, {8, 3, 4}},
        {{2, 7, 6}, {9, 5, 1}, {4, 3, 8}},
    };
    int minDifference = 9 * 8;
    for (int[][] magicSquare : magicSquares) {
      int difference = 0;
      for (int magicRow = 0; magicRow < s.length; magicRow++) {
        for (int magicColumn = 0; magicColumn < s.length; magicColumn++) {
          difference += Math.abs(magicSquare[magicRow][magicColumn] - s[magicRow][magicColumn]);
        }
      }
      if (difference < minDifference) {
        minDifference = difference;
      }
    }
    return minDifference;
  }

  public static void main(String[] args) {
    System.out.println(formingMagicSquare(new int[][]{{2, 9, 4}, {7, 5, 3}, {6, 1, 8}}));
    System.out.println(formingMagicSquare(new int[][]{{7, 5, 3}, {2, 9, 4}, {6, 1, 8}}));
  }

}
