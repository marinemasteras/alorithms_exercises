package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class PalindromeIndex {

  static int palindromeIndex(String s) {
    int i = 0;
    int j = s.length() - 1;
    int result;
    while (i < j) {
      if (s.charAt(i) != s.charAt(j)) {
        break;
      }
      i++;
      j--;
    }
    result = -1;

    if (s.charAt(i + 1) == s.charAt(j)) {
      int m = i + 1;
      int n = j;
      result = i;
      while (m < n) {
        if (s.charAt(m) != s.charAt(n)) {
          result = -1;
          break;
        }
        m++;
        n--;
      }
      if (m >= n) {
        return result;
      }
    }
    if (s.charAt(i) == s.charAt(j - 1)) {
      int m = i;
      int n = j - 1;
      result = j;
      while (m < n) {
        if (s.charAt(m) != s.charAt(n)) {
          result = -1;
          break;
        }
        m++;
        n--;
      }
    }
    return result;
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));
    int q = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    for (int qItr = 0; qItr < q; qItr++) {
      String s = scanner.nextLine();

      int result = palindromeIndex(s);
      System.out.println(result);
    }
  }
}
