package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class DFSnoRecursion {


  public DFSnoRecursion(Graph graph, int start) {
    this.marked = new boolean[graph.V];
    this.edgeTo = new int[graph.V];
    this.s = start;
  }

  private static class Graph {

    private final int V;
    private final ArrayList<Integer>[] adj;

    public Graph(int V) {
      this.V = V;
      adj = (ArrayList<Integer>[]) new ArrayList[V];
      for (int v = 0; v < V; v++) {
        adj[v] = new ArrayList<>();
      }
    }

    public void addEdge(int v, int w) {
      adj[v].add(w);
      adj[w].add(v);
    }

    public Iterable<Integer> adj(int v) {
      return adj[v];
    }
  }

  private boolean[] marked;
  private int[] edgeTo;
  private int s;

  public void dfsNoRecursion(Graph G, int v) {
    Stack<Integer> verticesToBeChecked = new Stack<>();
    verticesToBeChecked.add(v);

    while (!verticesToBeChecked.isEmpty()) {
      Integer i = verticesToBeChecked.pop();
      marked[i] = true;
      for (Integer child : G.adj(i)) {
        if (!marked[child]) {
          edgeTo[child] = i;
          verticesToBeChecked.push(child);
        }
      }
    }
    System.out.println(Arrays.toString(marked));
    System.out.println(Arrays.toString(edgeTo));


  }

  private void dfs(Graph G, int v) {
    marked[v] = true;
    for (int w : G.adj(v)) {
      if (!marked[w]) {
        dfs(G, w);
        edgeTo[w] = v;
      }
    }
    System.out.println(Arrays.toString(marked));
    System.out.println(Arrays.toString(edgeTo));
  }


  public static void main(String[] args) {
    Graph g1 = new Graph(5);
    g1.addEdge(1, 0);
    g1.addEdge(0, 2);
    g1.addEdge(2, 1);
    g1.addEdge(1, 3);
    g1.addEdge(3, 4);
    g1.addEdge(1, 4);
    DFSnoRecursion d = new DFSnoRecursion(g1, 0);
    d.dfsNoRecursion(g1, 0);
    d.dfs(g1, 0);
  }
}
