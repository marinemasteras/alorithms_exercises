package org.example;

import java.util.HashSet;
import java.util.Set;

public class StringConstruction {
  static int stringConstruction(String s) {
    Set<Character> charSet = new HashSet<>();
    for (int i = 0; i< s.length(); i++){
      charSet.add(s.charAt(i));
    }
    return charSet.size();
  }
}
