package org.example;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GridlandMetro {

  static long gridlandMetro(int n, int m, int k, int[][] track) {

    Arrays.sort(track, (a, b) -> Integer.compare(a[0], b[0]));

    int checkedRow = 1;
    int comparedRow = 0;
    long railSum = 0;

    while (checkedRow < n + 1) {
      if (comparedRow < k && checkedRow == track[comparedRow][0]) {
        List<int[]> aa = new ArrayList<>();
        while (comparedRow < k && checkedRow == track[comparedRow][0]) {
          if (aa.isEmpty()) {
            aa.add(new int[]{track[comparedRow][1], track[comparedRow][2]});
          } else {
            for (int[] section : aa) {
              if (doesIntersect(track[comparedRow][1], track[comparedRow][2], section[0],
                  section[1])) {
                section[0] = Math.min(section[0], track[comparedRow][1]);
                section[1] = Math.max(section[1], track[comparedRow][2]);
                break;
              }
              if (aa.indexOf(section) == aa.size() - 1) {
                aa.add(new int[]{track[comparedRow][1], track[comparedRow][2]});
                break;
              }
            }
          }
          comparedRow++;
        }
        for (int[] section : aa) {
          railSum += (long) section[1] - (long) section[0] + 1;
        }
      }
      checkedRow++;
    }
    long counter = (long) m * (long) n - railSum;
    return counter;
  }

  private static boolean doesIntersect(int aStart, int aEnd, int bStart, int bEnd) {
    return aStart <= bEnd && bStart <= aEnd;
  }


  public static void main(String[] args) throws FileNotFoundException {
    //System.out.println(gridlandMetro(4, 4, 3, new Integer[][]{{2, 2, 3,}, {3, 1, 4}, {4, 4, 4}}));
    Scanner scanner = new Scanner(new File("src/input10.txt"));
    int m = scanner.nextInt();
    int n = scanner.nextInt();
    int k = scanner.nextInt();

    int[][] track = new int[k][3];
    for (int j = 0; j < k; j++) {
      int[] row = new int[3];
      for (int i = 0; i < 3; i++) {
        row[i] = scanner.nextInt();
      }
      track[j] = row;
    }
    gridlandMetro(m, n, k, track);
  }
}
