package org.example;

import java.io.IOException;

public class DivisibleSumPairCounter {

  static int divisibleSumPairs(int n, int k, int[] ar) {
    int[] restBuckets = new int[k];
    int count = 0;

    for (int i = 0; i < n; i++) {
      int rest = ar[i] % k;
      if (rest == 0) {
        count = count + restBuckets[rest];
      } else {
        count = count + restBuckets[k - rest];
      }
      restBuckets[rest] = restBuckets[rest] + 1;
    }
    return count;
  }


  public static void main(String[] args) throws IOException {
    System.out.println(divisibleSumPairs(6, 3, new int[]{1, 3, 2, 6, 1, 2}));
  }
}
