package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class CountLuck {

  static String countLuck(String[] matrix, int k) {

    if (matrix.length == 0) {
      throw new IllegalArgumentException();
    }

    int rows = matrix.length;
    int columns = matrix[0].length();
    Cell[][] maze = new Cell[rows + 2][columns + 2];
    Stack<Cell> path = new Stack<>();

    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < columns; c++) {
        char ch = matrix[r].charAt(c);
        if (isOpen(ch)) {
          Cell cell = new Cell(r, c, ch, false);
          if (ch == 'M') {
            path.push(cell);
          }
          maze[r + 1][c + 1] = cell;
        }
      }
    }
    int counter = 0;
    Cell root = path.peek();
    root.checked = true;
    Stack<Cell> neighbours = getNeighbours(root, maze);
    for (Cell neighbour : neighbours) {
      if (!neighbour.checked) {
        search(neighbour, path, maze, counter);
      }
    }
    path.pop();
    while (path.size() > 1) {
      Cell x = path.pop();
      if (getNeighbours(x, maze).size() > 2) {
        counter++;
      }
    }
    Cell x = path.pop();
    if (getNeighbours(x, maze).size() >= 2) {
      counter++;

    }
    //System.out.println(counter);
    if (k == counter) {
      return "Impressed";
    }
    return "Oops!";

  }

  private static void search(Cell cell, Stack<Cell> path, Cell[][] maze, int counter) {
    cell.checked = true;
    path.push(cell);
    if (path.peek().ch == '*') {
      return;
    }
    Stack<Cell> neighbours = getNeighbours(cell, maze);
    for (Cell neighbour : neighbours) {
      if (!neighbour.checked) {
        search(neighbour, path, maze, counter);
      }
    }
    if (path.peek().ch != '*') {
      path.pop();
    }
  }

  private static Stack<Cell> getNeighbours(Cell root, Cell[][] maze) {
    Stack<Cell> neighbours = new Stack<>();
    if (maze[root.x][root.y + 1] != null) {
      neighbours.push(maze[root.x][root.y + 1]);
    }
    if (maze[root.x + 1][root.y + 2] != null) {
      neighbours.push(maze[root.x + 1][root.y + 2]);
    }
    if (maze[root.x + 2][root.y + 1] != null) {
      neighbours.push(maze[root.x + 2][root.y + 1]);
    }
    if (maze[root.x + 1][root.y] != null) {
      neighbours.push(maze[root.x + 1][root.y]);
    }
    return neighbours;
  }

  private static boolean isOpen(char ch) {
    return ch != 'X';
  }

  private static class Cell {

    int x;
    int y;
    char ch;
    boolean checked;

    public Cell(int x, int y, char ch, boolean checked) {
      this.x = x;
      this.y = y;
      this.ch = ch;
      this.checked = checked;
    }
  }


  public static void main(String[] args) throws FileNotFoundException {
/*    System.out.println(countLuck(new String[]{".X.X......X", ".X*.X.XXX.X", ".XX.X.XM...", "......XXXX."}, 2));
    System.out.println(countLuck(new String[]{"*.M", ".X."}, 2));
    System.out.println( countLuck(new String[]{"*.M...",".X.X.X","XXX..."}, 1));
    System.out.println(  countLuck(new String[]{"*..M..",".X.X.X","XXX..."}, 2));
    System.out.println(  countLuck(new String[]{"*M....",".X.X.X","XXX..."}, 1));
    System.out.println( countLuck(new String[]{"*....M",".X.X.X","XXX..."}, 2));
    System.out.println( countLuck(new String[]{"*.....",".X.X.X","XXX.M."}, 3));*/

    Scanner scanner = new Scanner(new File("src/input00.txt"));
    int count = scanner.nextInt();
    for (int j = 0; j < count; j++) {
      int x = scanner.nextInt();
      int y = scanner.nextInt();
      scanner.nextLine();
      String[] arr = new String[x];
      for (int i = 0; i < x; i++) {
        arr[i] = scanner.nextLine();
      }
      int k = scanner.nextInt();
      System.out.println(countLuck(arr, k));
      if (scanner.hasNext()) {
        scanner.nextLine();
      }
    }
  }
}
