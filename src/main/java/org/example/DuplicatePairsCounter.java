package org.example;

import java.util.Arrays;

public class DuplicatePairsCounter {


  public static void main(String[] args) {
    int[] a = new int[]{1, 2, 3, 1, 1, 4, 5, 6, 2, 5, 4, 2, 6, 4, 3, 1, 5, 7, 9, 5, 3, 2, 13, 5, 67,
        4, 3, 23, 321, 3, 6, 2, 35, 63, 3, 2, 5, 7, 7, 4, 3, 2, 1, 4};
    System.out.println(countDuplicatePairsNLogN(a));
    System.out.println(countDuplicatePairsON2(a));
  }

  public static int countDuplicatePairsON2(int[] ints) {
    int length = ints.length;
    int count = 0;
    for (int i = 0; i < length; i++) {
      for (int j = i + 1; j < length - 1; j++) {
        if (ints[i] == ints[j]) {
          count++;
        }
      }
    }
    return count;
  }

  public static int countDuplicatePairsNLogN(int[] ints) {
    Arrays.sort(ints);
    int count = 0;
    int i = 0;
    int j = 1;
    while (j < ints.length) {
      int miniCount = 0;
      while (j <= ints.length && ints[i] == ints[j]) {
        miniCount++;
        count += miniCount;
        j++;
      }
      i = j;
      j++;
    }
    return count;
  }

}

