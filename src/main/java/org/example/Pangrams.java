package org.example;

import java.util.HashSet;
import java.util.Set;

public class Pangrams {

  static String pangrams(String s) {
    //ascii table decimal values for lower cases: 97 - 122 and 65-90 for uppers
    //hasLetter[0] will be 'a' or 'A'
    boolean[] hasLetter = new boolean[26];
    int counter = 26;
    int index = 0;

    while (index < s.length()) {
      int y = (s.charAt(index));
      int i = y - 97;
      if (i > -1) {
        if (!hasLetter[i]) {
          hasLetter[i] = true;
          counter--;
        }
      } else if (i > -32) {
        if (!hasLetter[i + 32]) {
          hasLetter[i + 32] = true;
          counter--;
        }
      }
      index++;
      if (counter == 0) {
        return "pangram";
      }

    }
    return "not pangram";
  }

  static String pangrams2(String s) {
    String input = s.replaceAll(" ", "").toLowerCase();
    Set<Character> letters = new HashSet<>();
    int index = 0;
    while (index < input.length()) {
      letters.add(input.charAt(index));
      index++;
    }
    if (letters.size() == 26) {
      return "pangram";
    }
    return "not pangram";
  }

  public static void main(String[] args) {
    System.out.println(pangrams("We promptly judged antique ivory buckles for the next prize"));
    System.out.println(pangrams2("We promptly judged antique ivory buckles for the next prize"));
  }
}
