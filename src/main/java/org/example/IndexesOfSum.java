package org.example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class IndexesOfSum {

  static int[] icecreamParlor(int m, int[] arr) {
    int[] helperArr = new int[m + 1];
    int[] results = new int[2];
    for (int i = 0; i < arr.length; i++) {
      if (arr[i] < m) {
        if (helperArr[arr[i]] != 0) {
          if (helperArr[m - arr[i]] != 0) {
            results[0] = helperArr[arr[i]];
            results[1] = i + 1;
            break;
          }
        }
        helperArr[arr[i]] = i + 1;
        if (helperArr[m - arr[i]] != 0 && m != 2 * arr[i]) {
          results[0] = helperArr[m - arr[i]];
          results[1] = helperArr[arr[i]];
          break;
        }
      }
    }
    System.out.println(Arrays.toString(results));
    return results;
  }

  static int[] icecreamParlorMap(int m, int[] arr) {
    int[] result = new int[2];
    Map<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < arr.length; i++) {
      int x = arr[i];
      int y = m - x;
      Integer j = map.get(y);
      if (j != null) {
        result[0] = j + 1;
        result[1] = i + 1;
        break;
      }
      map.put(x, i);

    }
    System.out.println(Arrays.toString(result));
    return result;
  }

  public static void main(String[] args) {
    icecreamParlor(4, new int[]{1, 2, 1, 2, 1, 3, 3, 2, 5, 5});
    icecreamParlorMap(4, new int[]{1, 2, 1, 2, 1, 3, 3, 2, 5, 5});
  }
}
