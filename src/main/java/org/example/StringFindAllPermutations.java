package org.example;

import java.util.HashSet;
import java.util.Set;

public class StringFindAllPermutations {

  public static Set<String> permutationFinder(String str) {
    Set<String> perm = new HashSet<>();
    if (str == null) {
      return null;
    } else if (str.length() == 0) {
      perm.add("");
      return perm;
    }
    char initial = str.charAt(0);
    String rem = str.substring(1);
    Set<String> words = permutationFinder(rem);
    for (String strNew : words) {
      for (int i = 0; i <= strNew.length(); i++) {
        perm.add(charInsert(strNew, initial, i));
      }
    }
    return perm;
  }

  public static String charInsert(String str, char c, int j) {
    String begin = str.substring(0, j);
    String end = str.substring(j);
    return begin + c + end;
  }

  public static Set<String> permutationFinder2(String str) {
    Set<String> perm = new HashSet<>();
    char[] charArr = str.toCharArray();
    permute(charArr, 0, str.length() - 1, perm);
    return perm;
  }

  public static void permute(char[] charArr, int start, int end, Set<String> resultSet) {
    if (start == end) {
      resultSet.add(new String(charArr));
      return;
    }
    for (int i = start; i <= end; i++) {
      swap(charArr, start, i);
      permute(charArr, start + 1, end, resultSet);
      swap(charArr, start, i);
    }
  }

  static void swap(char[] arr, int a, int b) {
    char temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
  }

  public static void main(String[] args) {
    String s = "AAC";
    String s1 = "ABC";
    String s2 = "ABCD";
    System.out.println("\nPermutations for " + s + " are: \n" + permutationFinder(s));
    System.out.println("\nPermutations for " + s + " are: \n" + permutationFinder2(s));
    System.out.println("\nPermutations for " + s1 + " are: \n" + permutationFinder(s1));
    System.out.println("\nPermutations for " + s1 + " are: \n" + permutationFinder2(s1));
    System.out.println("\nPermutations for " + s2 + " are: \n" + permutationFinder(s2));
    System.out.println("\nPermutations for " + s2 + " are: \n" + permutationFinder2(s2));
  }
}
