package org.example;

public class SquaresInRange {


  static int squares(int a, int b) {
    int c = (int) Math.ceil(Math.sqrt(b));
    int d = (int) (Math.floor(Math.sqrt(a)));
    return ((int) (Math.floor(Math.sqrt(b))) - (int) (Math.ceil(Math.sqrt(a))) + 1);

  }

  public static void main(String[] args) {
    System.out.println(squares(17, 24));
  }
}
