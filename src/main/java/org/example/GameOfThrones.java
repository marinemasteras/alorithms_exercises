package org.example;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class GameOfThrones {

  static String gameOfThrones(String s) {
    int[] repetitions = new int[26];
    for (int i = 0; i < s.length(); i++) {
      int c = s.charAt(i) - 97;
      if (c > 25) {
        System.out.println(c);
      }
      repetitions[c]++;
    }
    int odds = 0;
    for (int j = 0; j < 26; j++) {
      if (repetitions[j] % 2 == 1) {
        odds++;
        if (odds > 1) {
          return "NO";
        }
      }
    }
    return "YES";
  }

  public static void main(String[] args) throws IOException {

    Scanner scanner = new Scanner(new File("src/got.txt"));
    String s = scanner.nextLine();
    String result = gameOfThrones(s);
    System.out.println(result);
  }

}