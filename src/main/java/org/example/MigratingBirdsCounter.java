package org.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MigratingBirdsCounter {

  static int migratoryBirds(List<Integer> arr) {
    long[] counts = new long[5];
    for (Integer birdType : arr) {
      counts[birdType - 1]++;
    }
    int max = 0;
    for (int i = 0; i < counts.length; i++) {
      if (counts[i] > counts[max]) {
        max = i;
      }
    }
    return max + 1;
  }

  public static void main(String[] args) throws IOException {

    List<Integer> ints = new ArrayList<>();
    for (int a = 0; a < 12214992; a++) {
      ints.add(5);
      ints.add(3);
      //ints.add((int) (Math.random() * 10) / 2 + 1);
    }
    System.out.println(migratoryBirds(ints));

  }
}
