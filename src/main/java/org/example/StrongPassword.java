package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StrongPassword {

  static int minimumNumber(int n, String password) {
    // Return the minimum number of characters to make the password strong
    int counter = 0;
    String[] regexes = new String[]{"[a-z]", "[A-Z]", "[0-9]", "[!@#$%^&*()--+ ]"};
    for (String regex : regexes) {
      if (password.replaceAll(regex, "").length() != password.length()) {
        counter++;
      }
    }
    if(password.length()>5){
    return regexes.length-counter;}
    else{
      return Math.max(6-password.length(), regexes.length-counter);
    }
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));
    int n = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    String password = scanner.nextLine();

    int answer = minimumNumber(n, password);

    System.out.println(answer);
  }

}
