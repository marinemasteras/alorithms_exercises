package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class EulerCycle {

  private final int V;
  private final ArrayList<Integer>[] adj;

  public EulerCycle(int V) {
    this.V = V;
    this.adj = new ArrayList[V];
    for (int v = 0; v < V; v++) {
      adj[v] = new ArrayList<>();
    }
  }

  void addEdge(int v, int w)
  {
    adj[v].add(w);
    adj[w].add(v);
  }

  private List<Integer> getEulerCycleRecursive(int v){
    List<Integer> results = new ArrayList<>();
    boolean[] visited = new boolean[V];
    DFSeuler(v,visited,results);
    return results;
  }


  void DFSeuler(int v,boolean[] visited, List<Integer> results)
  {

    if(adj[v].isEmpty()){
      visited[v] = true;
      results.add(v);
      return;
    }
    while (!adj[v].isEmpty())
    {
      int child = adj[v].get(adj[v].size()-1);
      if (!visited[child]) {
        deleteEdge(v,child);
        DFSeuler(child, visited,results);
        System.out.println("im in "+ v + " after eulering my child: "+ child);
        if(adj[v].isEmpty()){
          visited[v] = true;
          results.add(v);
          return;
        }
      }
    }
  }

  void deleteEdge(int v, int w){
    adj[v].remove((Integer) w);
    adj[w].remove((Integer) v);
  }

  public void findEulerCycle(int root){
    for(int i=0;i<V;i++){
      if (adj[i].size() %2 != 0){
        System.out.println("cannot create Euler circuit");
        return;
      }
    }
    List<Integer> results = getEulerCycle(root);
    System.out.println(results);
    List<Integer> resultsRecursive = getEulerCycleRecursive(root);
    System.out.println(resultsRecursive);
  }

  private List<Integer> getEulerCycle(int root) {
    List<Integer> results = new ArrayList<>();
    Stack<Integer> stack = new Stack<>();
    stack.push(root);
    while (!stack.isEmpty()){
      Integer v = stack.peek();
      if(adj[v].size() == 0){
        results.add(stack.pop());
      }
      else{
        int indexOfLastChild=adj[v].size()-1;
        Integer child = adj[v].get(indexOfLastChild);
        deleteEdge(v,child);
        stack.push(child);
      }
    }
    return results;
  }

  public static void test(EulerCycle g){
    for (int i=0; i< g.V; i++){
      ArrayList<Integer> edges = g.adj[i];
      System.out.println("vertice: "+ i + ", edges: " + edges.toString());
    }
  }

  public static void main(String[] args) {
    EulerCycle g1 = new EulerCycle(5);
    g1.addEdge(1, 0);
    g1.addEdge(0, 2);
    g1.addEdge(2, 1);
    g1.addEdge(1, 3);
    g1.addEdge(3, 4);
    g1.addEdge(1, 4);
    test(g1);
    g1.findEulerCycle(0);

    EulerCycle g2 = new EulerCycle(7);
    g2.addEdge(1, 0);
    g2.addEdge(1, 2);
    g2.addEdge(1, 3);
    g2.addEdge(3, 4);
    g2.addEdge(3, 5);
    g2.addEdge(5, 6);
    g2.addEdge(4, 5);
    test(g2);
    g2.findEulerCycle(0);

  }

}
