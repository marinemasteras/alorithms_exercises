package org.example;

import java.util.Arrays;
import java.util.Stack;

public class ClosestPairs {

  public static void main(String[] args) {
    int[] a = new int[]{2,0,-4,-3916237, -357920,-357918, -3620601, 7374819, -7330761, 30, 6246457, -6461594,
        266854, -10, -20, 10,20,99999999,99999498};
    System.out.println(Arrays.toString(closestNumbers(a)));
  }


  static int[] closestNumbers(int[] arr) {
    Arrays.sort(arr);
    Stack<Integer> stack = new Stack<>();
    stack.push(0);
    int minDifference = arr[1] - arr[0];
    for (int i = 2; i < arr.length; i++) {
      if (arr[i] - arr[i - 1] < minDifference) {
        minDifference=arr[i] - arr[i - 1];
        stack.clear();
        stack.push(i-1);
      }
      else if (arr[i] - arr[i - 1] == minDifference){
        stack.push(i-1);
      }
    }
    int[] results = new int[2*stack.size()];
    for (int i = results.length-2; i>-1 ; i=i-2){
      int index = stack.pop();
      results[i]=arr[index];
      results[i+1]=arr[index+1];
    }
    return results;
  }
}
