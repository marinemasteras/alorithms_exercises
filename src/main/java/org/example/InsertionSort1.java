package org.example;

public class InsertionSort1 {

  static void insertionSort1(int n, int[] arr) {
    int numberToSort = arr[n - 1];
    int indexToMove = arr.length - 2;
    while (indexToMove > -1 && arr[indexToMove] > numberToSort) {
      int numberToMove = arr[indexToMove];
      arr[indexToMove + 1] = numberToMove;
      printOutput(arr);
      indexToMove--;
    }
    arr[indexToMove + 1] = numberToSort;
    printOutput(arr);
  }

  private static void printOutput(int[] arr) {
    StringBuilder s = new StringBuilder();
    for (int i : arr) {
      s.append(i).append(" ");
    }
    System.out.println(s);
  }

  public static void main(String[] args) {
    insertionSort1(5, new int[]{2, 4, 6, 8, 1});
  }
}
