package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Gemstones {

  static int gemstones(String[] arr) {
    int length = arr.length;
    if (length == 1) {
      return length;
    }
    Set<Character> comparisonSet = makeSet(arr[0]);
    while (length > 0) {
      length--;
      Set<Character> checkedSet = makeSet(arr[length]);
      comparisonSet.retainAll(checkedSet);
    }
    return comparisonSet.size();
  }

  private static Set<Character> makeSet(String s) {
    Set<Character> checkedSet = new HashSet<>();
    for (char c : s.toCharArray()) {
      checkedSet.add(c);
    }
    return checkedSet;
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));

    int n = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    String[] arr = new String[n];

    for (int i = 0; i < n; i++) {
      String arrItem = scanner.nextLine();
      arr[i] = arrItem;
    }

    int result = gemstones(arr);
    System.out.println(result);
  }

}
