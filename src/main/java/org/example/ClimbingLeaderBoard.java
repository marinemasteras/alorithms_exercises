package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClimbingLeaderBoard {


  public static List<Integer> climbingLeaderboard(List<Integer> ranked, List<Integer> player) {
    // Write your code here

    List<Integer> noDuplicateList = ranked.stream().distinct().collect(Collectors.toList());
    List<Integer> rankingHistory = new ArrayList<>();
    int loopStart = noDuplicateList.size();
    for (Integer playerScore : player) {
      for (int rankingValueIndex = loopStart - 1; rankingValueIndex > -1; rankingValueIndex--) {
        int rankingValue = noDuplicateList.get(rankingValueIndex);
        if (rankingValue > playerScore) {
          loopStart = rankingValueIndex + 1;
          break;
        } else {
          loopStart = rankingValueIndex;
          continue;
        }
      }
      rankingHistory.add(loopStart + 1);
    }
    return rankingHistory;
  }

  public static List<Integer> climbingLeaderboardBinarySearch(List<Integer> ranked,
      List<Integer> player) {
    // Write your code here

    List<Integer> noDuplicateList = ranked.stream().distinct().collect(Collectors.toList());
    List<Integer> rankingHistory = new ArrayList<>();
    int from = 0;
    int to = noDuplicateList.size() - 1;
    for (int lookUp : player) {
      int valueIndex = binarySearch(lookUp, from, to, noDuplicateList);
      to = Math.min(valueIndex, noDuplicateList.size() - 1);
      rankingHistory.add(valueIndex + 1);
    }
    return rankingHistory;
  }

  private static int binarySearch(int lookUp, int from, int to, List<Integer> ranked) {

    while (from < to) {
      int half = ((to + from) / 2);
      if (lookUp < ranked.get(half)) {
        return binarySearch(lookUp, half + 1, to, ranked);
      } else if (lookUp > ranked.get(half)) {
        return binarySearch(lookUp, from, half, ranked);
      } else {
        return half;
      }
    }
    if (lookUp < ranked.get(to)) {
      return to + 1;
    }
    return (to);
  }


  private static void insertionSort(List<Integer> ranked) {
    for (int i = 1; i < ranked.size(); i++) {
      Integer a = ranked.get(i);

      Integer j = i;
      while (j > 0 && ranked.get(j - 1) < a) {
        ranked.set(j, ranked.get(j - 1));
        j--;
      }
      ranked.set(j, a);
    }
  }


  public static void main(String[] args) {
    List ranked = new ArrayList();
    List player = new ArrayList();
    List ranked2 = List.of(100, 100, 50, 40, 40, 20, 10);
    List player2 = List.of(4, 4, 4, 5, 15, 25, 50, 120, 120, 120);
    ranked2.forEach(a -> ranked.add(a));
    player2.forEach(a -> player.add(a));

    System.out.println(climbingLeaderboard(ranked, player));
    System.out.println(climbingLeaderboardBinarySearch(ranked, player));

  }
}
