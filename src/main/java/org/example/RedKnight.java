package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.LinkedTransferQueue;

public class RedKnight {

  static class Field {

    int x;
    int y;
    Field previous;
    String moveTo;

    public Field(int x, int y, Field previous, String moveTo) {
      this.x = x;
      this.y = y;
      this.previous = previous;
      this.moveTo = moveTo;
    }
  }

  static void printShortestPath(int n, int i_start, int j_start, int i_end, int j_end) {
    int counter = 0;
    Stack<String> path = new Stack<>();
    int[] dx = {-2, -2, 0, 2, 2, 0};
    int[] dy = {-1, 1, 2, 1, -1, -2};
    String[] moveTo = {"UL", "UR", "R", "LR", "LL", "L"};

    if ((i_start % 2 == 0 && i_end % 2 != 0) || (i_start % 2 != 0 && i_end % 2 == 0)) {
      System.out.println("Impossible");
      return;
    }
    if (((i_start - i_end + 2) % 4 == 0 && (j_start - j_end) % 2 == 0) ||
        ((i_start - i_end) % 4 == 0 && (j_start - j_end) % 2 != 0)) {
      System.out.println("Impossible");
      return;
    }

    boolean[][] marked = new boolean[n][n];
    Queue<Field> queue = new LinkedTransferQueue<>();
    queue.add(new Field(i_start, j_start, null, null));
    marked[i_start][j_start] = true;

    while (!queue.isEmpty()) {
      Field v = queue.remove();
      if (v.x == i_end && v.y == j_end) {
        while (v.previous != null) {
          counter++;
          path.push(v.moveTo);
          v = v.previous;
        }
        break;
      }
      Iterable<Field> adjacents = getAdjacents(v, dx, dy, n, moveTo);
      for (Field adjacent : adjacents) {
        if (!marked[adjacent.x][adjacent.y]) {
          queue.add(adjacent);
          marked[adjacent.x][adjacent.y] = true;
        }
      }
    }
    System.out.println(counter);
    while (!path.isEmpty()) {
      System.out.print(path.pop() + " ");
    }
  }

  static Iterable<Field> getAdjacents(Field v, int[] dx, int[] dy, int n, String[] moveTo) {
    List<Field> adjacents = new ArrayList<>();
    for (int i = 0; i < 6; i++) {
      if (0 <= v.x + dx[i] && v.x + dx[i] < n && 0 <= v.y + dy[i] && v.y + dy[i] < n) {
        adjacents.add(new Field(v.x + dx[i], v.y + dy[i], v, moveTo[i]));
      }
    }
    return adjacents;
  }


  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input01.txt"));
    int n = scanner.nextInt();
    int i_start = scanner.nextInt();
    int j_start = scanner.nextInt();
    int i_end = scanner.nextInt();

    int j_end = scanner.nextInt();
    printShortestPath(n, i_start, j_start, i_end, j_end);
  }
}
