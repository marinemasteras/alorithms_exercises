package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FunnyString {

  static String funnyString(String s) {
    int index = s.length() - 1;
    for (int i = 0; i < index / 2; i++) {
      if (Math.abs(s.charAt(i) - s.charAt(i + 1)) != Math
          .abs(s.charAt(index - i) - s.charAt(index - i - 1))) {
        return "Not Funny";
      }
    }
    return "Funny";
  }

  public static void main(String[] args) throws FileNotFoundException {
    Scanner scanner = new Scanner(new File("src/input00.txt"));
    int q = scanner.nextInt();
    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

    for (int qItr = 0; qItr < q; qItr++) {
      String s = scanner.nextLine();

      String result = funnyString(s);
      System.out.println(result);
    }
  }
}
