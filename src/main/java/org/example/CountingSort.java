package org.example;

public class CountingSort {

  static int[] countingSort(int[] arr) {
    int[] sorted = new int[100];
    for (int i = 0; i < arr.length; i++) {
      sorted[arr[i]]++;
    }
    return sorted;
  }
}
