package org.example;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class MinimumLoss {

  static int minimumLoss(long[] price) {
    Map<Long, Integer> priceYearMap = new TreeMap<>();
    for (int i = 0; i < price.length; i++) {
      priceYearMap.put(price[i], i);
    }
    Iterator<Long> it = priceYearMap.keySet().iterator();
    Long a = it.next();
    long c = 1000000L;
    while (it.hasNext()) {
      Long b = it.next();

      if (b - a < c && priceYearMap.get(b) < priceYearMap.get(a)) {
        c = b - a;
      }

      a = b;
    }
    return Math.toIntExact(c);
  }
}
