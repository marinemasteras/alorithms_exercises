public class TwoStrings {

  static String twoStrings(String s1, String s2) {
    int[] s1Alphabet = new int[26];
    for (int i = 0; i < s1.length(); i++) {
      int chVal = getCharValue(s1, i);
      s1Alphabet[chVal] += 1;
    }
    for (int i = 0; i < s2.length(); i++) {
      int chVal = getCharValue(s2, i);
      if (s1Alphabet[chVal] > 0) {
        return "YES";
      }
    }
    return "NO";
  }

  private static int getCharValue(String s2, int i) {
    return s2.charAt(i) - 97;
  }

  public static void main(String[] args) {
    System.out.println(twoStrings("hello", "ziom"));
  }
}
